# Expotech 2024 Workshop Machine learning

- Objetivo general:  Al finalizar el curso el estudiante debe ser capaz de construir modelos de aprendizaje de máquina que le permitan resolver problemas complejos por medio de datos validados y patrones establecidos en la literatura científica.

- Objetivos específicos:

1.    Identificar los conceptos de los modelos supervisados y no supervisados

2.    Analizar las fases de los modelos supervisados y no supervisados

3.   Desarrollar modelos supervisados y no supervisados para problemas complejos

- Herramientas:

- Google colab https://colab.research.google.com/
- Gitlab https://about.gitlab.com/
- Chatgpt https://openai.com/blog/chatgpt

- Repositorio del curso
- https://gitlab.com/ejimenez/expotech_2024_machine_learning.git

- Libros
- Challenges in DeployingMachine Learning: a Survey of Case Studies
- Data Mining for the Masses
- The Elements of Statistical Learning
- MATHEMATICS FOR MACHINE LEARNING

Parte 1 ##############################################################################################

**Presentación**

- ¿Qué es machine learning?
- Historia
- Introducción
- Aplicaciones 
- Mercado laboral
- Metodología CRISP-DM
- Algoritmos de predicción
- Regresión lineal
- Predicción - https://colab.research.google.com/drive/1Wfw8ztpkOhp8vxB-1ROtzgWYRylBD1qT#scrollTo=bjrEA8uMcwxg
- Ejemplo 1 - https://colab.research.google.com/drive/1nLSTVffim8DK3bycsA462pp8UJPjZqwK?usp=sharing
- Ejemplo 2 - https://colab.research.google.com/drive/1ApOc9sorayJpqJuCffQR8u7ilEBn-gOA?usp=sharing
- Ejemplo 3 - https://colab.research.google.com/drive/12yNZE8ij7fn8Th5AdI9C7S3WaAgoMTrT?usp=sharing

Parte 2 ##############################################################################################

- Algoritmos de predicción
- **Serie de tiempo**
- Predicción - https://colab.research.google.com/drive/1kfyxOBsYY-GbjQmf_MK86KibwCDVRTke?usp=sharing
- Ejemplo 1 - https://colab.research.google.com/drive/1BQf_cVLFRcfDl2MQ_233Yp-SUp7JPeqw?usp=sharing
- Ejemplo 2 - https://colab.research.google.com/drive/10d1LMWvuaLerdEBIntFlKxhmNlmkte0O?usp=sharing
- Ejemplo 3 - https://colab.research.google.com/drive/1o9w8Eu8bfyGXhOHxJHY_stlcdri4q0Og?usp=sharing

Parte 3 ##############################################################################################

- Algoritmos de clasificacion
- **Arboles de decision y Bosques aleatorios**
- Clasificación - https://colab.research.google.com/drive/1mLMfzn8CfPE1gG9GUsGippyRivzN6ldh?usp=sharing
- Ejemplo 1 - https://colab.research.google.com/drive/1UTmEAlfOKJdQuBrWrojPAI8NJiUufOoP?usp=sharing
- Ejemplo 2 - https://colab.research.google.com/drive/1-3dz-bhVKE6buec18w6fuOrFnATgQNkg?usp=sharing
- Ejemplo 3 - https://colab.research.google.com/drive/1eQXkzMAx6viyWu7x3eQKYZsR_IsrDA6V?usp=sharing
- K**NN**
- Clasificación - https://colab.research.google.com/drive/1YS94DJEsLQD-XDW9pQouLtkzlXLxZnad?usp=sharing
- Ejemplo 4 - https://colab.research.google.com/drive/1pNacH0fXYr35oMi8B4n71QmeBoSmIObf?usp=sharing
- **SVM**
- Clasificación - https://colab.research.google.com/drive/1LiFu_3WWK0eTTU0JUzK_gZFTgb-7n95x?usp=sharing
- Ejemplo 5 - https://colab.research.google.com/drive/1rNODR01FC82MjXogwl-iDX2xQ9C8wASg?usp=sharing

Parte 4 ##############################################################################################


- Algoritmos de clasificacion
- **Naive Bayes**
- Clasificación - https://colab.research.google.com/drive/1fhLNsRI8cBAEY2kbFMpPfBKDiGxnX5Um?usp=sharing
- Ejemplo 6 - https://colab.research.google.com/drive/1cHgiZDGfNMqDQoQ_ldR4luSzuA3_P2DJ?usp=sharing
- **Logistic Regression**
- Clasificación - https://colab.research.google.com/drive/1KNXy8-HxhnBr75XTufGxcXW19AOwDeQm?usp=sharing
- Ejemplo 7 - https://colab.research.google.com/drive/1M5G63i5MhpidBcb1bCRiG2NzpFXWy5qH?usp=sharing
- **Red Neuronal Artificial**
- Clasificación - https://colab.research.google.com/drive/1bnlEIT0Ta_Hc5Z3b1kxu5ioaqDrvpJAQ?usp=sharing
- Ejemplo 8 - https://colab.research.google.com/drive/1aC0Z0e_jJNGB8ajZXz5eLRX_BhAiV23S?usp=sharing
- Ejemplo 9 - https://colab.research.google.com/drive/1HdXx7nqJX_1m60CekMSvRvzyhltj8WCM?usp=sharing


Parte 5 ##############################################################################################


**Algoritmos No supervisado**
- Agrupación
- Kmeans Ejemplo 1 - https://colab.research.google.com/drive/1qoh-6ikE1knBEi5usv_raAZCBwbp4_mL?usp=sharing
- Kmeans Ejemplo 2 -https://colab.research.google.com/drive/1PorQE21BowmFaE52d32m3RVTWNhhZa2q?usp=sharing
- Kmeans Ejemplo 3 -https://colab.research.google.com/drive/1o8tiyM--WmfFYcPiXGLM0tQ3XzgCkD-d?usp=sharing
- Asociacion 
- Apriori  Ejemplo 4 - https://colab.research.google.com/drive/1MGQU7Is3xEXFSroiN_YZQ52lVn9ELFFx?usp=sharing
- Apriori  Ejemplo 5 - https://colab.research.google.com/drive/1y9_V92lZg119-RC6SE2N9sIkBLokI6iA?usp=sharing
- Apriori  Ejemplo 6 - https://colab.research.google.com/drive/13eSKtf8zg5oR8giGu77isN2YvRSqSJb_?usp=sharing

##############################################################################################

- Dataset
- https://www.kaggle.com/
- https://archive.ics.uci.edu/ml/index.php


- Bibliografía  

- Introduction to Machine Learning with Python: A Guide for Data Scientists 
-      1st Edition Andreas Müller  

-       ISBN-13: 978-1449369415

 -      ISBN-10: 1449369413

- Machine Learning with PyTorch and Scikit-Learn: Develop machine learning and deep learning models with Python Sebastian Raschka
- ISBN-13: 978-1801819312


- ISBN-10: 1801819319

- AI and Machine Learning for Coders: A Programmer's Guide to Artificial Intelligence 
1st Edition 


- Laurence Moroney


- ISBN-13: 978-1492078197


- ISBN-10: 1492078190


